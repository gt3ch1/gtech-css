A basic color library for css, with some QOL mods.

To use:
   - Download gtech-css or copy the link for it, then add this line to your document
   - `<link rel="stylesheet" href="gtech-css.css"> `


Demo:
    Please look at demo.html to see all the colors

All of these colors are based off of my photographs on instagram, @gcpease.
